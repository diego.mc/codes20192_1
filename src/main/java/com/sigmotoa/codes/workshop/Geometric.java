package com.sigmotoa.codes.workshop;
/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Geometric Exercises
 */


import static java.lang.Math.*;

public class Geometric {

//Calculate the area for a square using one side
public static int squareArea(int side)
{
    int area = side*side;
    System.out.println(area);
    return area;
}

//Calculate the area for a square using two sides
public static int squareArea(int sidea, int sideb) 
{
    int area = sidea*sideb;
    System.out.println(area);
    return area;
}

//Calculate the area of circle with the radius
public static double circleArea(double radius)
{
    double radio= Math.PI * 2 *radius;
    System.out.println(radio) ;
    return radio;
}

//Calculate the perimeter of circle with the diameter
public static double circlePerimeter(int diameter)
{
    double  c=( 2 *Math.PI *diameter) /2;
    System.out.println(c);
    return c;
}
    //*************************************************************************************

//Calculate the perimeter of square with a side
public static double squarePerimeter(double side)
{
    double  perimeter= side*side ;
    System.out.println(perimeter);
    return perimeter;
}

//Calculate the volume of the sphere with the radius
public static double sphereVolume(double radius)
{
    double volume = (4.0 / 3) * Math.PI * radius * radius * radius;
    System.out.println(volume);
    return volume;
}

//Calculate the area of regular pentagon with one side
public static float pentagonArea(int side)
{
    double area = (sqrt(5 * (5 + 2 * (sqrt(5)))) * side * side) / 4;
    //float area_pentagon=(5.0/2.0)*side*side;
    System.out.print(area);

    return (int)area;
}

//Calculate the Hypotenuse with two cathetus
public static double calculateHypotenuse(double catA, double catB)
{
    double c=Math.sqrt((catA*catA+catB*catB));
    System.out.println(c);
    return c;
}

}
