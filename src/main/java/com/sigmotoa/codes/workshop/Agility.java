package com.sigmotoa.codes.workshop;

import jdk.swing.interop.SwingInterOpUtils;

import java.beans.PropertyEditorSupport;
import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * @author sigmotoa
 *
 * This class contains some popular brain games with numbers
 */
public class Agility {

    public static boolean biggerThan(String numA, String numB) {
        Double a = Double.parseDouble(numA);
        Double b = Double.parseDouble(numB);
        if (a > b) {
            System.out.println(a);
            return true;
        } else
            System.out.println(numB);
        return false;
    }

    //Sort from bigger the numbers an show in array
    //Ordenar de mayor a mayor los números de un espectáculo en matriz
    public static int[] order(int numA, int numB, int numC, int numD, int numE) {
        int[] all = {numA, numB, numC, numD, numE};
        Arrays.sort(all);
        Arrays.toString(all);
        System.out.println("mayor a mayor los números de un espectáculo en matriz" + Arrays.toString(all));
        return all;
    }

    //Look for the smaller number of the array
    // Busca el número más pequeño de la matriz
    public static double smallerThan(double array[]) {
        Arrays.sort(array);
        System.out.println("El número más pequeño de la matriz es: " + array[0]);
        return array[0];
    }


    // Se llama número de palíndromo en español capicúa
    // El número es palíndromo
    //Palindrome number is called in Spanish capicúa
    //The number is palindrome

    public static boolean palindromeNumber(Integer numA) {
        int aux, inverso, cifra;
        inverso = 0;
        aux = numA;
        while (aux != 0) {
            cifra = aux % 10;
            inverso = inverso * 10 + cifra;
            aux = aux / 10;
        }
        System.out.println(inverso);

        if (numA == inverso) {
            return true;
        }
        return false;
    }


    // la palabra es palíndromo
    //the word is palindrome
    public static boolean palindromeWord(String word) {
        String reversedWord = "";
        for (int i = word.length() - 1; i >= 0; i--) {
            reversedWord = reversedWord + word.charAt(i);
        }
        if (word.equals(reversedWord)) {
            return true;
        } else {
            return false;
        }
    }


    //Show the factorial number for the parameter
    // Muestra el número factorial para el parámetro
    public static int factorial(int numA) {
        int num = numA;
        int factorial = 1;
        for (int i = num; i > 0; i--) {
            factorial = factorial * i;
        }
        System.out.println("El factorial de " + num + " es: " + factorial);
        return 0;

    }

    // es el número impar
    //is the number odd
    public static boolean isOdd(byte numA) {
        if (numA % 2 != 0) {
            System.out.println("El número impar: " + numA);
            return true;
        } else
            return false;
    }

    //is the number prime
    // es el número primo
    public static boolean isPrimeNumber(int numA) {
        int suma = 0, i;
        for (i = 1; i < numA; i++) {
            if (numA % i == 0) {
                suma = suma + i;
            }
        }
        if (suma == numA) {
            return true;
        } else {
            return false;
        }
    }

    //is the number even
    //es el numero par
    public static boolean isEven(byte numA) {
        if (numA % 2 == 0) {
            System.out.println("El numero par es: " + numA);
            return true;
        } else
            return false;
    }

    //is the number perfect
    public static boolean isPerfectNumber(int numA) {
        return false;
    }

    //Return an array with the fibonacci sequence for the requested number
    public static int[] fibonacci(int numA) {
        return new int[0];
    }


    //how many times the number is divided by 3
    // cuántas veces se divide el número entre 3
    public static int timesDividedByThree(int numA) {
        int a, b, c;
        a = 3;
        b = numA % a;
        c = numA / a;
        if (b == 0) {
            System.out.println(c);
            return c;
        } else {
            return c;
        }
    }

    //The game of fizzbuzz
    public static String fizzBuzz(int numA)

    // * If number is divided by 3, show fizz
    //* If number is divided by 5, show buzz
    //* If number is divided by 3 and 5, show fizzbuzz
    //* in other cases, show the number

    {
        String mensaje;
        if (numA % 3 == 0 && numA % 5 == 0) {
            mensaje = "show fizzbuzz";
            //System.out.println(mensaje);
            return mensaje;
        }
        else{
            if (numA % 3 == 0) {
                mensaje = "fizz";
                //   System.out.println(mensaje);
                return mensaje;

            }
            else{
                if (numA % 5 == 0) {
                    mensaje = "buzz";
                //   System.out.println(mensaje);
                return mensaje;

                }
                else {
                    mensaje = new String(String.valueOf(numA));
                //  System.out.println(mensaje);
                return mensaje;

                }
            }
        }
    }
}