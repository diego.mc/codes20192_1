package com.sigmotoa.codes.workshop;

/**
 * @author sigmotoa
 *
 * @version 1
 *
 * Convertion Exercises
 */
public class Convertion {

    //convert of units of Metric System

//Km to metters
    public static int kmToM1(double km)
    {
        double result = km * 1000;
        System.out.println(result);
        {
            return (int)result;
        }
    }

//Km to metters
    public static double kmTom(double km)
    {
        double result = km * 1000;
        System.out.println(result);
        {
            return (int)result;
        }
    }
    //Km to cm
    public static double kmTocm(double km)
    {
        double result = km / 0.000010000;
        System.out.println(result);
        {
            return (int)result;
        }
    }

//milimetters to metters
    public static double mmTom(int mm)
    {
        double result = mm / 1000.0;
        System.out.println(result);
        {
            return (double)result;
        }
    }



//convert of units of U.S Standard System

//convert miles to foot
    public static double milesToFoot(double miles)
    {
        double result = miles * 5280;
        System.out.println(result);
        {
            return (double)result;
        }
    }

    //convert yards to inches
    public static int yardToInch(int yard)
    {
        int result = yard * 36;
        System.out.println(result);
        {
            return (int) result;
        }
    }
    
    //convert inches to miles
    public static double inchToMiles(double inch)
    {
        double result = inch / 63360;
        System.out.println(result);
        {
            return (double) result;
        }
    }

//convert foot to yards
    public static int footToYard(int foot)
    {
        int result = foot / 3;
        System.out.println(result);
        {
            return (int) result;
        }
    }

//Convert units in both systems

//convert Km to inches
    public static double kmToInch(String km)
    {
        Double a = Double.parseDouble(km);

        Double result = a * 39370.079;
        System.out.println(result);
        {
            return (Double) result;
        }
    }

//convert milimmeters to foots
    public static double mmToFoot(String mm)
    {
        Double a = Double.parseDouble(mm);

        Double result = a /304.8 ;
        System.out.println(result);
        {
            return (Double) result;
        }
    }

//convert yards to cm    
    public static double yardToCm(String yard)
    {
        Double a = Double.parseDouble(yard);

        Double result = a * 91.44;
        System.out.println(result);
        {
            return (Double) result;
        }
    }
    }
